
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlBonoboInt.h"

#include "BonoboDefs.h"

#if 0
typedef void  (*BonoboPersistStreamIOFn) (BonoboPersistStream         *ps,
					  const Bonobo_Stream         stream,
					  Bonobo_Persist_ContentType  type,
					  void                       *closure,
					  CORBA_Environment          *ev);

typedef CORBA_long (*BonoboPersistStreamMaxFn) (BonoboPersistStream *ps,
						void                *closure,
						CORBA_Environment   *ev);

typedef Bonobo_Persist_ContentTypeList * (*BonoboPersistStreamTypesFn) (BonoboPersistStream *ps,
									void                *closure,
									CORBA_Environment   *ev);
#endif

MODULE = Gnome::BonoboPersistStream		PACKAGE = Gnome::BonoboPersistStream		PREFIX = bonobo_persist_stream_

#ifdef BONOBO_PERSIST_STREAM

void
bonobo_persist_stream_set_dirty (ps, dirty)
	Gnome::BonoboPersistStream	ps
	bool	dirty

Gnome::BonoboPersistStream
bonobo_persist_stream_new (load_fn, save_fn, max_fn, types_fn, closure)
	SV *	load_fn
	SV *	save_fn
	SV *	max_fn
	SV *	types_fn
	CODE:
	RETVAL = NULL;
	OUTPUT:
	RETVAL

#endif

