
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlBonoboInt.h"

#include "BonoboDefs.h"

MODULE = Gnome::BonoboPlug		PACKAGE = Gnome::BonoboPlug		PREFIX = bonobo_plug_

#ifdef BONOBO_PLUG


Gtk::Widget_Sink
bonobo_plug_new (Class, socket_id)
	SV *	Class
	guint32	socket_id
	CODE:
	RETVAL = (BonoboPlug*)(bonobo_plug_new (socket_id));
	OUTPUT:
	RETVAL

#endif

