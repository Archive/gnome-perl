
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlBonoboInt.h"

#include "BonoboDefs.h"
#include "GtkDefs.h"
#include "MiscTypes.h"

static void
render_func (GnomePrintContext *ctx, double width, double height, const Bonobo_PrintScissor *opt_scissor, gpointer data) {
	SV * handler;
	AV * args = (AV*)data;
	int i;
	dSP;

	handler = * av_fetch(args, 0, 0);
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(ctx), NULL)));
	XPUSHs(sv_2mortal(newSVnv(width)));
	XPUSHs(sv_2mortal(newSVnv(height)));
	XPUSHs(sv_2mortal(newSVnv(opt_scissor->width_first_page)));
	XPUSHs(sv_2mortal(newSVnv(opt_scissor->width_per_page)));
	XPUSHs(sv_2mortal(newSVnv(opt_scissor->height_first_page)));
	XPUSHs(sv_2mortal(newSVnv(opt_scissor->height_per_page)));
	for (i=1;i<=av_len(args);i++)
		XPUSHs(sv_2mortal(newSVsv(*av_fetch(args, i, 0))));
	PUTBACK;
	perl_call_sv(handler, G_DISCARD);

}

MODULE = Gnome::BonoboPrint		PACKAGE = Gnome::BonoboPrint		PREFIX = bonobo_print_

#ifdef BONOBO_PRINT

Gnome::BonoboPrint
bonobo_print_new (Class, handler, ...)
	SV *	Class
	SV *	handler
	CODE:
	{
		AV * args = newAV();
		PackCallbackST(args, 1);
		RETVAL = bonobo_print_new (render_func, args);
		/* FIXME args leak */
	}
	OUTPUT:
	RETVAL

#endif

