
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlBonoboInt.h"

#include "BonoboDefs.h"

MODULE = Gnome::BonoboSocket		PACKAGE = Gnome::BonoboSocket		PREFIX = bonobo_socket_

#ifdef BONOBO_SOCKET


Gtk::Widget_Sink
bonobo_socket_new (Class)
	SV *	Class
	CODE:
	RETVAL = (BonoboSocket*)(bonobo_socket_new ());
	OUTPUT:
	RETVAL

void
bonobo_socket_set_control_frame (socket, frame)
	Gnome::BonoboSocket	socket
	Gnome::BonoboControlFrame	frame


#endif

