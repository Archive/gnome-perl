
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlBonoboInt.h"

#include "BonoboDefs.h"

MODULE = Gnome::BonoboStream		PACKAGE = Gnome::BonoboStream		PREFIX = bonobo_stream_

#ifdef BONOBO_STREAM

Gnome::BonoboStream
bonobo_stream_open (Class, driver, path, flags, mode)
	SV *	Class
	char *	driver
	char *	path
	int	flags
	int	mode
	CODE:
	TRY(RETVAL=bonobo_stream_open_full (driver, path, flags, mode, &ev));
	OUTPUT:
	RETVAL

#endif

