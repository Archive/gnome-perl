
package Gnome::Gal;

require Gtk;
require Gtk::Gdk::ImlibImage;
require Gtk::Gdk::Pixbuf;
require Gnome;
require Exporter;
require DynaLoader;

$VERSION = '0.7009';

@ISA = qw(Exporter DynaLoader);
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT = qw(
        
);
# Other items we are prepared to export if requested
@EXPORT_OK = qw(
);

sub dl_load_flags {Gtk::dl_load_flags()}

bootstrap Gnome::Gal $VERSION;

if ($Gnome::Gal::lazy) {
	require Gnome::Gal::TypesLazy;
} else {
	require Gnome::Gal::Types;
	&Gnome::Gal::_boot_all();
}

# Autoload methods go after __END__, and are processed by the autosplit program.

Gtk->mod_init_add('Gnome', sub {
	init Gnome::Gal;
});

# provide some handy default handlers
package Gnome::ETableSimple;

sub is_cell_editable_impl {
	return 1;
}

sub value_is_empty_impl {
	my ($self, $col, $val) = @_;
	return 1 if (!defined($val) || !length($val));
	return 0;
}

sub value_to_string_impl {
	my ($self, $col, $val) = @_;
	return $val;
}

1;
__END__
