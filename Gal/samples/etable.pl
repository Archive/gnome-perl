#!/usr/bin/perl -w
use Gtk::lazy;
use Gnome::Gal;

init Gnome 'etable';

# You need basically 4 functions in your derived table model class:
# 1) a constructor that creates a Gnome::ETableSimple and blesses it
# in your own package
# 2) row_count_impl () returns the number of rows
# 3) value_at_impl ($col, $row) gets the value of the cell
# 4) set_value_at_impl ($col, $row) sets the value of the cell
# When you change your data, you need to signal it with
# $model->changed;
# or other less heavy-weight variants (row_changed, row_inserted, row_deleted, etc...).
# That's it!
# Maybe I should get rid of the _impl suffix? bah
package mytable;
@mytable::ISA = 'Gnome::ETableSimple';

sub new {
	my ($class, $cols, @data) = @_;
	my $self = new Gnome::ETableSimple(@$cols);
	$self->{data} = \@data;
	return bless $self, ref($class)||$class;
}

sub row_count_impl {
	my $self = shift;
	return scalar(@{$self->{data}});
}

sub value_at_impl {
	# this needs to be fast: it's called a lot
	return $_[0]->{data}->[$_[2]]->[$_[1]];
}

sub set_value_at_impl {
	my ($self, $col, $row, $val) = @_;
	#warn "set value at: $row $col: $val\n";
	$self->{data}->[$row]->[$col] = $val;
}

sub is_cell_editable_impl {
	my ($self, $col, $row) = @_;
	return $col != 1;
}

sub append {
	my ($self, $row) = @_;
	my $r = @{$self->{data}};
	push(@{$self->{data}}, $row);
	$self->row_inserted ($r);
}

package main;

my $SPEC = '<ETableSpecification cursor-mode="line" selection-mode="browse" draw-focus="true">
  <ETableColumn model_col="0" _title="Name"   expansion="1.0" minimum_width="20" resizable="true" cell="string" compare="string"/>
  <ETableColumn model_col="1" _title="Burp" expansion="1.0" minimum_width="20" resizable="true" cell="string"      compare="string"/>
  <ETableColumn model_col="2" _title="Toggle"     expansion="1.0" minimum_width="20" resizable="true" cell="checkbox"      compare="integer"/>
  <ETableColumn model_col="3" _title="Number"      expansion="1.0" minimum_width="20" resizable="true" cell="number"      compare="integer"/>
        <ETableState>
	        <column source="0"/>
	        <column source="1"/>
	        <column source="2"/>
	        <column source="3"/>
	        <grouping> <leaf column="1" ascending="true"/> </grouping>
        </ETableState>
</ETableSpecification>
';

my $win = new Gtk::Window;
$win->signal_connect('destroy', sub {Gtk->main_quit});
my $table_model = new mytable(['GtkString', 'GtkString', 'gboolean', 'gint'], 
	[qw(1 2 1 4)], 
	[qw(ciao hippo 0 5)]);

my $frame = new Gtk::Frame;
my $etable = new Gnome::ETableScrolled($table_model, undef, $SPEC);
$frame->add($etable);
$win->add($frame);
$win->set_usize(200, 200);
$win->show_all;
Gtk->timeout_add(5000, sub {
	$table_model->append([qw(cippa lippa 0), rand(20)]);
	return 1;
});
Gtk->main;

