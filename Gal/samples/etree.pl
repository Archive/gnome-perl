#!/usr/bin/perl -w
use Gtk::lazy;
use Gnome::Gal;

init Gnome 'etree';

package MyTree;

@MyTree::ISA = 'Gnome::ETreeMemoryCallbacks';

sub new {
  my $class = shift;
  my $self = new Gnome::ETreeMemoryCallbacks( ('GtkString', 'GtkString') );
  return bless $self, ref($class) || $class;
}

sub value_at_impl {
  my $self = shift;
  my $path = shift;
  my $col = shift;

  return "Path $path Col $col";
}

sub set_value_at_impl {
  my $self = shift;
  my $path = shift;
  my $col = shift;
  my $val = shift;

  print "set_value_at $path $col $v\n";
}

sub is_cell_editable_impl {
  return 0;
}

package main;

my $SPEC = <<EOSPEC
<ETableSpecification cursor-mode="line" selection-mode="browse" draw-focus="true">
 <ETableColumn model_col="0" _title="Col 0" expansion="1.0" width="30" resizable="true" cell="tree-string" compare="string"/>
 <ETableColumn model_col="1" _title="Col 1" expansion="1.0" width="30" resizable="true" cell="string" compare="string"/>
 <ETableState>
  <column source="0"/>
  <column source="1"/>
  <grouping></grouping>
 </ETableState>
</ETableSpecification>
EOSPEC
;

my $win = new Gtk::Window;
$win->signal_connect ('destroy', sub { Gtk->main_quit; } );
my $etmm = new MyTree;
my $adapter = new Gnome::ETreeTableAdapter ($etmm);
$adapter->load_expanded_state ("expanded_state");

$root_node = $etmm->node_insert (undef, 0, undef);

$adapter->root_node_set_visible;

foreach my $i (1 .. 5) {
  $subn = $etmm->node_insert ($root_node, 0, "First level $i");
  foreach my $j (1 .. 5) {
    $etmm->node_insert ($subn, 0, "Second level $j");
  }
}

my $frame = new Gtk::Frame;
my $etable = new Gnome::ETable ($adapter, undef, $SPEC, "");
$frame->add($etable);
$win->add($frame);
$win->set_usize(200, 200);
$win->show_all;
Gtk->main;
