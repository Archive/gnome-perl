
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"

#if 0

typedef void		(*ECanvasItemReflowFunc)		(GnomeCanvasItem *item,
								 gint   	  flags);

typedef void            (*ECanvasItemSelectionFunc)             (GnomeCanvasItem *item,
								 gint             flags,
								 gpointer         user_data);
/* Returns the same as strcmp does. */
typedef gint            (*ECanvasItemSelectionCompareFunc)      (GnomeCanvasItem *item,
								 gpointer         data1,
								 gpointer         data2,
								 gint             flags);

void e_canvas_item_set_reflow_callback (GnomeCanvasItem *item, ECanvasItemReflowFunc func);

void e_canvas_item_set_selection_callback (GnomeCanvasItem *item, ECanvasItemSelectionFunc func);
void e_canvas_item_set_selection_compare_callback (GnomeCanvasItem *item, ECanvasItemSelectionCompareFunc func);

void e_canvas_item_set_cursor (GnomeCanvasItem *item, gpointer id);
void e_canvas_item_add_selection (GnomeCanvasItem *item, gpointer id);
void e_canvas_item_remove_selection (GnomeCanvasItem *item, gpointer id);

void e_canvas_item_grab_focus (GnomeCanvasItem *item);

void e_canvas_item_request_reflow (GnomeCanvasItem *item);
void e_canvas_item_request_parent_reflow (GnomeCanvasItem *item);

#endif

MODULE = Gnome::ECanvas		PACKAGE = Gnome::ECanvas		PREFIX = e_canvas_

#ifdef E_CANVAS

Gnome::ECanvas_Sink
e_canvas_new (Class)
	SV *	Class
	CODE:
	RETVAL = e_canvas_new ();
	OUTPUT:
	RETVAL

#endif

