
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"

MODULE = Gnome::ECellText		PACKAGE = Gnome::ECellText		PREFIX = e_cell_text_

#ifdef E_CELL_TEXT

Gnome::ECell
e_cell_text_new (Class, fontname, justify)
	SV *	Class
	char *	fontname
	Gtk::Justification	justify
	CODE:
	RETVAL = e_cell_text_new (fontname, justify);
	OUTPUT:
	RETVAL

#endif

