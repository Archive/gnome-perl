
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"
#include "GtkTypes.h"

MODULE = Gnome::ETable		PACKAGE = Gnome::ETable		PREFIX = e_table_

#ifdef E_TABLE

Gnome::ETable_Sink
e_table_new (Class, etm, ete, spec, state=0)
	SV *	Class
	Gnome::ETableModel	etm
	Gnome::ETableExtras_OrNULL	ete
	char *	spec
	char *	state
	CODE:
	RETVAL = e_table_new (etm, ete, spec, state[0] == 0 ? NULL : state);
	OUTPUT:
	RETVAL

Gnome::ETable_Sink
e_table_new_from_spec_file (Class, etm, ete, spec_fn, state_fn=0)
	SV *	Class
	Gnome::ETableModel	etm
	Gnome::ETableExtras_OrNULL	ete
	char *	spec_fn
	char *	state_fn
	CODE:
	RETVAL = e_table_new_from_spec_file (etm, ete, spec_fn, state_fn);
	OUTPUT:
	RETVAL

char *
e_table_get_state (e_table)
	Gnome::ETable	e_table

void
e_table_save_state (e_table, filename)
	Gnome::ETable	e_table
	char *	filename

Gnome::ETableState
e_table_get_state_object (e_table)
	Gnome::ETable	e_table

void
e_table_set_state (e_table, state)
	Gnome::ETable	e_table
	char *	state

void
e_table_set_state_object (e_table, state)
	Gnome::ETable	e_table
	Gnome::ETableState	state

void
e_table_load_state (e_table, filename)
	Gnome::ETable	e_table
	char *	filename

void
e_table_set_cursor_row (e_table, row)
	Gnome::ETable	e_table
	int	row

int
e_table_get_cursor_row (e_table)
	Gnome::ETable	e_table

#if 0

# void
# e_table_selected_row_foreach (e_table, callback, closure)
#	Gnome::ETable	e_table
#	Gnome::ETableForeachFunc	callback
#	gpointer	closure

#endif

int
e_table_selected_count (e_table)
	Gnome::ETable	e_table

Gnome::EPrintable
e_table_get_printable (e_table)
	Gnome::ETable	e_table

int
e_table_get_next_row (e_table, model_row)
	Gnome::ETable	e_table
	int	model_row

int
e_table_get_prev_row (e_table, model_row)
	Gnome::ETable	e_table
	int	model_row

int
e_table_model_to_view_row (e_table, model_row)
	Gnome::ETable	e_table
	int	model_row

int
e_table_view_to_model_row (e_table, view_row)
	Gnome::ETable	e_table
	int	view_row

void
e_table_drag_get_data (table, row, col, context, target, time)
	Gnome::ETable	table
	int	row
	int	col
	Gtk::Gdk::DragContext	context
	Gtk::Gdk::Atom	target
	guint32	time

void
e_table_drag_highlight (table, row, col)
	Gnome::ETable	table
	int	row
	int	col

void
e_table_drag_unhighlight (table)
	Gnome::ETable	table

#if 0

void
e_table_drag_dest_set (table, flags, targets, n_targets, actions)
	Gnome::ETable	table
	Gtk::DestDefaults	flags
	Gtk::TargetEntry	targets
	int	n_targets
	Gtk::Gdk::DragAction	actions

#endif

void
e_table_drag_dest_set_proxy (table, proxy_window, protocol, use_coordinates)
	Gnome::ETable	table
	Gtk::Gdk::Window	proxy_window
	Gtk::Gdk::DragProtocol	protocol
	bool	use_coordinates

void
e_table_drag_dest_unset (widget)
	Gtk::Widget	widget

#if 0

void
e_table_drag_source_set (table, start_button_mask, targets, n_targets, actions)
	Gnome::ETable	table
	Gtk::Gdk::ModifierType	start_button_mask
	Gtk::TargetEntry	targets
	int	n_targets
	Gtk::Gdk::DragAction	actions

#endif

void
e_table_drag_source_unset (table)
	Gnome::ETable	table

Gtk::Gdk::DragContext
e_table_drag_begin (table, row, col, targets, actions, button, event)
	Gnome::ETable	table
	int	row
	int	col
	Gtk::TargetList	targets
	Gtk::Gdk::DragAction	actions
	int	button
	Gtk::Gdk::Event	event

void
e_table_select_all (table)
	Gnome::ETable	table

void
e_table_invert_selection (table)
	Gnome::ETable	table

#endif

