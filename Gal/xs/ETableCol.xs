
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GdkPixbufDefs.h"

#if 0
typedef enum {
	E_TABLE_COL_ARROW_NONE = 0,
	E_TABLE_COL_ARROW_UP,
	E_TABLE_COL_ARROW_DOWN
} ETableColArrow;
#endif

static int compare () {
	return 0;
}

MODULE = Gnome::ETableCol		PACKAGE = Gnome::ETableCol		PREFIX = e_table_col_

#ifdef E_TABLE_COL

Gnome::ETableCol
e_table_col_new (Class, col_idx, text, expansion, min_width, ecell, resizable)
	SV *	Class
	int	col_idx
	char *	text
	double	expansion
	int	min_width
	Gnome::ECell	ecell
	bool	resizable
	CODE:
	RETVAL = e_table_col_new (col_idx, text, expansion, min_width, ecell, compare, resizable);
	OUTPUT:
	RETVAL

Gnome::ETableCol
e_table_col_new_with_pixbuf (Class, col_idx, text, pixbuf, expansion, min_width, ecell, resizable)
	SV *	Class
	int	col_idx
	char *	text
	Gtk::Gdk::Pixbuf	pixbuf
	double	expansion
	int	min_width
	Gnome::ECell	ecell
	bool	resizable
	CODE:
	RETVAL = e_table_col_new_with_pixbuf (col_idx, text, pixbuf, expansion, min_width, ecell, compare, resizable);
	OUTPUT:
	RETVAL

void
e_table_col_destroy (etc)
	Gnome::ETableCol	etc

#endif

