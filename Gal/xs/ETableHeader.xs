
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"
#include "GtkTypes.h"

MODULE = Gnome::ETableHeader		PACKAGE = Gnome::ETableHeader		PREFIX = e_table_header_

#ifdef E_TABLE_HEADER

Gnome::ETableHeader
e_table_header_new (Class)
	SV *	Class
	CODE:
	RETVAL = e_table_header_new ();
	OUTPUT:
	RETVAL

void
e_table_header_add_column (eth, tc, pos)
	Gnome::ETableHeader	eth
	Gnome::ETableCol	tc
	int	pos

Gnome::ETableCol
e_table_header_get_column (eth, column)
	Gnome::ETableHeader	eth
	int	column

Gnome::ETableCol
e_table_header_get_column_by_col_idx (eth, col_idx)
	Gnome::ETableHeader	eth
	int	col_idx

int
e_table_header_count (eth)
	Gnome::ETableHeader	eth

int
e_table_header_index (eth, col)
	Gnome::ETableHeader	eth
	int	col

int
e_table_header_get_index_at (eth, x_offset)
	Gnome::ETableHeader	eth
	int	x_offset

#if 0

Gnome::ETableCol
e_table_header_get_columns (eth)
	Gnome::ETableHeader	eth

#endif

int
e_table_header_get_selected (eth)
	Gnome::ETableHeader	eth

int
e_table_header_total_width (eth)
	Gnome::ETableHeader	eth

void
e_table_header_move (eth, source_index, target_index)
	Gnome::ETableHeader	eth
	int	source_index
	int	target_index

void
e_table_header_remove (eth, idx)
	Gnome::ETableHeader	eth
	int	idx

void
e_table_header_set_size (eth, idx, size)
	Gnome::ETableHeader	eth
	int	idx
	int	size

void
e_table_header_set_selection (eth, allow_selection)
	Gnome::ETableHeader	eth
	bool	allow_selection

int
e_table_header_col_diff (eth, start_col, end_col)
	Gnome::ETableHeader	eth
	int	start_col
	int	end_col

void
e_table_header_calc_widths (eth)
	Gnome::ETableHeader	eth

void
e_table_header_get_selected_indexes (eth)
	Gnome::ETableHeader	eth
	PPCODE:
	{
		GList * l = e_table_header_get_selected_indexes (eth);
		/* ... */
	}


#endif

