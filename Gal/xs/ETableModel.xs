
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"

MODULE = Gnome::ETableModel		PACKAGE = Gnome::ETableModel		PREFIX = e_table_model_

#ifdef E_TABLE_MODEL

int
e_table_model_column_count (e_table_model)
	Gnome::ETableModel	e_table_model

char *
e_table_model_column_name (e_table_model, col)
	Gnome::ETableModel	e_table_model
	int	col

int
e_table_model_row_count (e_table_model)
	Gnome::ETableModel	e_table_model

#if 0

void *
e_table_model_value_at (e_table_model, col, row)
	Gnome::ETableModel	e_table_model
	int	col
	int	row


void
e_table_model_set_value_at (e_table_model, col, row, value)
	Gnome::ETableModel	e_table_model
	int	col
	int	row
	void *	value

void *
e_table_model_duplicate_value (e_table_model, col, value)
	Gnome::ETableModel	e_table_model
	int	col
	void *	value

void
e_table_model_free_value (e_table_model, col, value)
	Gnome::ETableModel	e_table_model
	int	col
	void *	value

void *
e_table_model_initialize_value (e_table_model, col)
	Gnome::ETableModel	e_table_model
	int	col

bool
e_table_model_value_is_empty (e_table_model, col, value)
	Gnome::ETableModel	e_table_model
	int	col
	void *	value

char *
e_table_model_value_to_string (e_table_model, col, value)
	Gnome::ETableModel	e_table_model
	int	col
	void *	value

#endif

bool
e_table_model_is_cell_editable (e_table_model, col, row)
	Gnome::ETableModel	e_table_model
	int	col
	int	row

void
e_table_model_append_row (e_table_model, source, row)
	Gnome::ETableModel	e_table_model
	Gnome::ETableModel	source
	int	row

char *
e_table_model_row_sort_group (e_table_model, row)
	Gnome::ETableModel	e_table_model
	int	row

bool
e_table_model_has_sort_group (e_table_model)
	Gnome::ETableModel	e_table_model

void
e_table_model_pre_change (e_table_model)
	Gnome::ETableModel	e_table_model

void
e_table_model_changed (e_table_model)
	Gnome::ETableModel	e_table_model

void
e_table_model_row_changed (e_table_model, row)
	Gnome::ETableModel	e_table_model
	int	row

void
e_table_model_cell_changed (e_table_model, col, row)
	Gnome::ETableModel	e_table_model
	int	col
	int	row

void
e_table_model_row_inserted (e_table_model, row)
	Gnome::ETableModel	e_table_model
	int	row

void
e_table_model_row_deleted (e_table_model, row)
	Gnome::ETableModel	e_table_model
	int	row

#endif

