
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"

MODULE = Gnome::ETableScrolled		PACKAGE = Gnome::ETableScrolled		PREFIX = e_table_scrolled_

#ifdef E_TABLE_SCROLLED

Gnome::ETableScrolled_Sink
e_table_scrolled_new (Class, etm, ete, spec, state=0)
	SV *	Class
	Gnome::ETableModel	etm
	Gnome::ETableExtras_OrNULL	ete
	char *	spec
	char *	state
	CODE:
	RETVAL = e_table_scrolled_new (etm, ete, spec, state);
	OUTPUT:
	RETVAL

Gnome::ETableScrolled_Sink
e_table_scrolled_new_from_spec_file (Class, etm, ete, spec_fn, state_fn=0)
	SV *	Class
	Gnome::ETableModel	etm
	Gnome::ETableExtras_OrNULL	ete
	char *	spec_fn
	char *	state_fn
	CODE:
	RETVAL = e_table_scrolled_new_from_spec_file (etm, ete, spec_fn, state_fn);
	OUTPUT:
	RETVAL

Gnome::ETable
e_table_scrolled_get_table (ets)
	Gnome::ETableScrolled	ets

#endif

