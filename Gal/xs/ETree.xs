
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"
#include "GtkTypes.h"

MODULE = Gnome::ETree		PACKAGE = Gnome::ETree		PREFIX = e_tree_

#ifdef E_TREE

Gnome::ETree_Sink
e_tree_new (Class, etm, ete, spec, state=0)
	SV *	Class
	Gnome::ETreeModel	etm
	Gnome::ETableExtras_OrNULL	ete
	char *	spec
	char *	state
	CODE:
	RETVAL = e_tree_new (etm, ete, spec, state);
	OUTPUT:
	RETVAL

Gnome::ETree_Sink
e_tree_new_from_spec_file (Class, etm, ete, spec_fn, state_fn=0)
	SV *	Class
	Gnome::ETreeModel	etm
	Gnome::ETableExtras_OrNULL	ete
	char *	spec_fn
	char *	state_fn
	CODE:
	RETVAL = e_tree_new_from_spec_file (etm, ete, spec_fn, state_fn);
	OUTPUT:
	RETVAL

char *
e_tree_get_state (e_tree)
	Gnome::ETree	e_tree

void
e_tree_save_state (e_tree, filename)
	Gnome::ETree	e_tree
	char *	filename

Gnome::ETableState
e_tree_get_state_object (e_tree)
	Gnome::ETree	e_tree

void
e_tree_set_state (e_tree, state)
	Gnome::ETree	e_tree
	char *	state

void
e_tree_set_state_object (e_tree, state)
	Gnome::ETree	e_tree
	Gnome::ETableState	state

void
e_tree_load_state (e_tree, filename)
	Gnome::ETree	e_tree
	char *	filename

Gnome::ETreePath
e_tree_get_cursor (e_tree)
	Gnome::ETree	e_tree

#if 0

# void
# e_tree_selected_row_foreach (e_tree, callback, closure)
#	Gnome::ETree	e_tree
#	Gnome::EForeachFunc	callback
#	gpointer	closure

#endif

#ifdef E_TREE_USE_TREE_SELECTION

# void
# e_tree_selected_path_foreach (e_tree, callback, closure)
#	Gnome::ETree	e_tree
#	Gnome::ETreeForeachFunc	callback
#	gpointer	closure

#endif

int
e_tree_selected_count (e_tree)
	Gnome::ETree	e_tree

Gnome::EPrintable
e_tree_get_printable (e_tree)
	Gnome::ETree	e_tree

int
e_tree_get_next_row (e_tree, model_row)
	Gnome::ETree	e_tree
	int	model_row

int
e_tree_get_prev_row (e_tree, model_row)
	Gnome::ETree	e_tree
	int	model_row

int
e_tree_model_to_view_row (e_tree, model_row)
	Gnome::ETree	e_tree
	int	model_row

int
e_tree_view_to_model_row (e_tree, view_row)
	Gnome::ETree	e_tree
	int	view_row

void
e_tree_drag_get_data (tree, row, col, context, target, time)
	Gnome::ETree	tree
	int	row
	int	col
	Gtk::Gdk::DragContext	context
	Gtk::Gdk::Atom	target
	guint32	time

void
e_tree_drag_highlight (tree, row, col)
	Gnome::ETree	tree
	int	row
	int	col

void
e_tree_drag_unhighlight (tree)
	Gnome::ETree	tree


#if 0

void
e_tree_drag_dest_set (tree, flags, targets, n_targets, actions)
	Gnome::ETree	tree
	Gtk::DestDefaults	flags
	Gtk::TargetEntry	targets
	int	n_targets
	Gtk::Gdk::DragAction	actions

#endif

void
e_tree_drag_dest_set_proxy (tree, proxy_window, protocol, use_coordinates)
	Gnome::ETree	tree
	Gtk::Gdk::Window	proxy_window
	Gtk::Gdk::DragProtocol	protocol
	bool	use_coordinates

void
e_tree_drag_dest_unset (widget)
	Gtk::Widget	widget

#if 0

void
e_tree_drag_source_set (tree, start_button_mask, targets, n_targets, actions)
	Gnome::ETree	tree
	Gtk::Gdk::ModifierType	start_button_mask
	Gtk::TargetEntry	targets
	int	n_targets
	Gtk::Gdk::DragAction	actions

#endif

void
e_tree_drag_source_unset (tree)
	Gnome::ETree	tree

Gtk::Gdk::DragContext
e_tree_drag_begin (tree, row, col, targets, actions, button, event)
	Gnome::ETree	tree
	int	row
	int	col
	Gtk::TargetList	targets
	Gtk::Gdk::DragAction	actions
	int	button
	Gtk::Gdk::Event	event

gboolean
e_tree_node_is_expanded (et, path)
	Gnome::ETree	et
	Gnome::ETreePath	path

void
e_tree_node_set_expanded (et, path, expanded=1)
	Gnome::ETree	et
	Gnome::ETreePath	path
	gboolean	expanded

void
e_tree_node_set_expanded_recurse (et, path, expanded=1)
	Gnome::ETree	et
	Gnome::ETreePath	path
	gboolean	expanded

void
e_tree_root_node_set_visible (et, visible=1)
	Gnome::ETree	et
	gboolean	visible

Gnome::ETreePath
e_tree_node_at_row (et, row)
	Gnome::ETree	et
	int	row

int
e_tree_row_of_node (et, path)
	Gnome::ETree	et
	Gnome::ETreePath	path

gboolean
e_tree_root_node_is_visible (et)
	Gnome::ETree	et

void
e_tree_show_node (et, path)
	Gnome::ETree	et
	Gnome::ETreePath	path

void
e_tree_save_expanded_state (et, filename)
	Gnome::ETree	et
	char *	filename

void
e_tree_load_expanded_state (et, filename)
	Gnome::ETree	et
	char *	filename

void
e_tree_row_count (et)
	Gnome::ETree	et

#endif /* E_TREE */
