
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"
#include "GtkTypes.h"

MODULE = Gnome::ETreeMemory		PACKAGE = Gnome::ETreeMemory		PREFIX = e_tree_memory_

#ifdef E_TREE_MEMORY

Gnome::ETreePath
e_tree_memory_node_insert (etree, parent, position, node_data)
	Gnome::ETreeMemory	etree
	Gnome::ETreePath	parent
	int	position
	char *	node_data
	CODE:
	RETVAL = e_tree_memory_node_insert (etree, parent, position, strdup(node_data));
	OUTPUT:
	RETVAL

Gnome::ETreePath
e_tree_memory_node_insert_before (etree, parent, sibling, node_data)
	Gnome::ETreeMemory	etree
	Gnome::ETreePath	parent
	Gnome::ETreePath	sibling
	char *	node_data
	CODE:
	RETVAL = e_tree_memory_node_insert_before (etree, parent, sibling, strdup(node_data));
	OUTPUT:
	RETVAL

void *
e_tree_memory_node_remove (etree, path)
	Gnome::ETreeMemory	etree
	Gnome::ETreePath	path

void
e_tree_memory_freeze (etree)
	Gnome::ETreeMemory	etree

void
e_tree_memory_thaw (etree)
	Gnome::ETreeMemory	etree

void
e_tree_memory_set_expanded_default (etree, expanded)
	Gnome::ETreeMemory	etree
	gboolean	expanded

char *
e_tree_memory_node_get_data (etm, node)
	Gnome::ETreeMemory	etm
	Gnome::ETreePath	node

void
e_tree_memory_node_set_data (etm, node, node_data)
	Gnome::ETreeMemory	etm
	Gnome::ETreePath	node
	char *	node_data
	CODE:
	e_tree_memory_node_set_data (etm, node, strdup(node_data));

#endif

