
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"
#include "GtkTypes.h"

static GdkPixbuf *
icon_at (ETreeModel *etm, ETreePath node, void *data)
{
	return NULL;
}

static GtkType
get_col_type (ETreeModel *etm, int col)
{
	GtkType * cols = gtk_object_get_data(GTK_OBJECT(etm), "perl-etm-types");
	if (!cols)
		return GTK_TYPE_STRING;
	return cols[col];
}

static gint
col_count (ETreeModel *etree, void *model_data)
{
	return GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(etree), "perl-etm-cols"));
}

static int 
idle_free (gpointer data) {
	g_free(data);
	return 0;
}

static void*
value_at (ETreeModel *etm, ETreePath path, int col, void *data)
{
	int i;
	GtkArg arg;
	dSP;

	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv((guint)path)));
	XPUSHs(sv_2mortal(newSViv(col)));
	PUTBACK;

	i = call_method("value_at_impl", G_SCALAR);

	SPAGAIN;
	if (i != 1)
		croak("big trouble!\n");
	arg.type = get_col_type (etm, col);
	GtkSetArg (&arg, POPs, NULL, NULL);
	PUTBACK;
	FREETMPS;
	LEAVE;

	if (arg.type == GTK_TYPE_STRING)
		gtk_idle_add(idle_free, GTK_VALUE_POINTER(arg));
	return GTK_VALUE_POINTER(arg);
}

static void
set_value_at (ETreeModel *etm, ETreePath path, int col, const void *val, void *data)
{
	dSP;
	GtkType type;
	SV *v;

	type = get_col_type (etm, col);
	/* val can be an int or a string or possibly other random types: 
	   we try to make a few guesses, but a segv is likely if we guess it wrong
	*/
	if (type == GTK_TYPE_BOOL) {
		v = newSViv((IV)val);
	} else {
		v = newSVpv((char*)val, 0);
	}
	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv((guint)path)));
	XPUSHs(sv_2mortal(newSViv(col)));
	XPUSHs(sv_2mortal(v));
	PUTBACK;

	call_method("set_value_at_impl", G_DISCARD);

	FREETMPS;
	LEAVE;
}

static gboolean
is_cell_editable (ETreeModel *etm, ETreePath path, int col, void *data) {
	int i;
	gboolean res;
	dSP;

	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv((guint)path)));
	XPUSHs(sv_2mortal(newSViv(col)));
	PUTBACK;

	i = call_method("is_cell_editable_impl", G_SCALAR);

	SPAGAIN;
	if (i != 1)
		croak("big trouble!\n");
	res = POPi;
	PUTBACK;
	FREETMPS;
	LEAVE;
	return res;
}

static void*
duplicate_value (ETreeModel *etm, int col, const void *val, void *data) {
	int i;
	SV* res;
	GtkType type;
	dSP;
	
	type = get_col_type (etm, col);
	if (type == GTK_TYPE_STRING)
		return g_strdup((char*)val);
	return val;

	/* this func deals only with C types it seems */
	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv(col)));
	/*XPUSHs(sv_2mortal(newSVsv((SV*)val)));*/
	PUTBACK;

	i = call_method("duplicate_value_impl", G_SCALAR);

	SPAGAIN;
	if (i != 1)
		croak("big trouble!\n");
	res = POPs;
	PUTBACK;
	FREETMPS;
	LEAVE;
	return val;
}

static void
free_value (ETreeModel *etm, int col, void *val, void *data) {
	dSP;

	/* this callback doesn't make sense */
	return;
	warn("free value %p\n", val);
	/* happily leak? */
	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv(col)));
	/*XPUSHs(sv_2mortal(newSVsv((SV*)val)));*/
	PUTBACK;

	call_method("free_value_impl", G_DISCARD);
	
	/*SvREFCNT_dec((SV*) val);*/
	FREETMPS;
	LEAVE;
	if (get_col_type (etm, col) == GTK_TYPE_STRING) {
		warn("free value %s\n", val);
		g_free(val);
	}
}

static void*
initialize_value (ETreeModel *etm, int col, void *data) {
	int i;
	SV* res;
	dSP;
	GtkType type;
	
	type = get_col_type (etm, col);
	if (type == GTK_TYPE_STRING)
		return g_strdup("");
	return NULL;

	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv(col)));
	PUTBACK;

	i = call_method("initialize_value_impl", G_SCALAR);

	SPAGAIN;
	if (i != 1)
		croak("big trouble!\n");
	res = newSVsv(POPs);
	PUTBACK;
	FREETMPS;
	LEAVE;
	return res;
}

static gboolean
value_is_empty (ETreeModel *etm, int col, const void *val, void *data) {
	int i;
	gboolean res;
	dSP;
	GtkArg arg;
	SV *v;

	arg.type = get_col_type (etm, col);
	GTK_VALUE_POINTER(arg) = val;
	v = GtkGetArg (&arg);

	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv(col)));
	XPUSHs(sv_2mortal(v));
	PUTBACK;

	i = call_method("value_is_empty_impl", G_SCALAR);

	SPAGAIN;
	if (i != 1)
		croak("big trouble!\n");
	res = SvTRUE(POPs);
	PUTBACK;
	FREETMPS;
	LEAVE;
	return res;
}

static char*
value_to_string (ETreeModel *etm, int col, const void *val, void *data) {
	int i;
	char* res;
	dSP;
	GtkArg arg;
	SV *v;

	arg.type = get_col_type (etm, col);
	GTK_VALUE_POINTER(arg) = val;
	v = GtkGetArg (&arg);

	ENTER;
	SAVETMPS;
	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVGtkObjectRef(GTK_OBJECT(etm), NULL)));
	XPUSHs(sv_2mortal(newSViv(col)));
	XPUSHs(sv_2mortal(v));
	PUTBACK;

	i = call_method("value_to_string_impl", G_SCALAR);

	SPAGAIN;
	if (i != 1)
		croak("big trouble!\n");
	res = g_strdup(SvPV(POPs, PL_na));
	PUTBACK;
	FREETMPS;
	LEAVE;
	return res;
}

MODULE = Gnome::ETreeMemoryCallbacks		PACKAGE = Gnome::ETreeMemoryCallbacks		PREFIX = e_tree_memory_callbacks_

#ifdef E_TREE_MEMORY_CALLBACKS

Gnome::ETreeMemoryCallbacks_Sink
new (Class, ...)
	SV *	Class
	CODE:
	{
		GtkType * cols = NULL;
		RETVAL = e_tree_memory_callbacks_new(icon_at, col_count,
			NULL, NULL, NULL, NULL,
			value_at, set_value_at, is_cell_editable,
			duplicate_value, free_value, initialize_value, value_is_empty, value_to_string, NULL);
		if (items > 1) {
			int i = items -1;
			int j;
			cols = g_new0(GtkType, i);
			for(j=0; j < i; ++j) {
				cols[j] = gtk_type_from_name(SvPV(ST(j+1), PL_na));
				if (!cols[j])
					cols[j] = GTK_TYPE_STRING;
			}
			gtk_object_set_data_full(GTK_OBJECT(RETVAL),  "perl-etm-types", cols, g_free);
			gtk_object_set_data(GTK_OBJECT(RETVAL),  "perl-etm-cols", GINT_TO_POINTER(i));
		} else {
			croak("need to provide column types");
		}
	}
	OUTPUT:
	RETVAL

#endif

