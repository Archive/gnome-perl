
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"
#include "GtkTypes.h"

MODULE = Gnome::ETreeModel		PACKAGE = Gnome::ETreeModel		PREFIX = e_tree_model_

#ifdef E_TREE_MODEL

Gnome::ETreePath
e_tree_model_get_root (etree)
	Gnome::ETreeModel	etree

Gnome::ETreePath
e_tree_model_node_get_parent (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

Gnome::ETreePath
e_tree_model_node_get_first_child (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

Gnome::ETreePath
e_tree_model_node_get_last_child (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

Gnome::ETreePath
e_tree_model_node_get_next (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

Gnome::ETreePath
e_tree_model_node_get_prev (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

gboolean
e_tree_model_node_is_root (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

gboolean
e_tree_model_node_is_expandable (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

#if 0

# guint
# e_tree_model_node_get_children (etree, path, paths)
#	Gnome::ETreeModel	etree
#	Gnome::ETreePath	path
#	Gnome::ETreePath**	path
#

#endif

guint
e_tree_model_node_depth (etree, path)
	Gnome::ETreeModel	etree
	Gnome::ETreePath	path

# if 0
# Gdk::Pixbuf
# e_tree_model_icon_at (etree, path)
#	Gnome::ETreeModel	etree
#	Gnome::ETreePath	path

#endif

gboolean
e_tree_model_get_expanded_default (model)
	Gnome::ETreeModel	model

gint
e_tree_model_column_count (model)
	Gnome::ETreeModel	model

gboolean
e_tree_model_has_save_id (model)
	Gnome::ETreeModel	model

char *
e_tree_model_get_save_id (model, node)
	Gnome::ETreeModel	model
	Gnome::ETreePath	node

gboolean
e_tree_model_has_get_node_by_id (model)
	Gnome::ETreeModel	model

Gnome::ETreePath
e_tree_model_get_node_by_id (model, save_id)
	Gnome::ETreeModel	model
	char *	save_id

gboolean
e_tree_model_has_change_pending (model)
	Gnome::ETreeModel	model

gboolean
e_tree_model_node_is_editable (model, node, col)
	Gnome::ETreeModel	model
	Gnome::ETreePath	node
	int	col

#if 0

void *
e_tree_model_value_at (model, node, col)
	Gnome::ETreeModel	model
	Gnome::ETreePath	node
	int	col

void
e_tree_model_set_value_at (model, node, col, value)
	Gnome::ETreeModel	model
	Gnome::ETreePath	node
	int	col
	void *	value


void *
e_tree_model_duplicate_value (etree, col, value)
	Gnome::ETreeModel	model
	int	col
	void *	value

void
e_tree_model_free_value (etree, col, value)
	Gnome::ETreeModel	model
	int	col
	void *	value

void *
e_tree_model_initialize_value (etree, col)
	Gnome::ETreeModel	model
	int	col

gboolean
e_tree_model_value_is_empty (etree, col, value)
	Gnome::ETreeModel	model
	int	col
	void *	value

char *
e_tree_model_value_to_string (etree, col, value)
	Gnome::ETreeModel	model
	int	col
	void *	value

#endif

#if 0

# void
# e_tree_model_node_traverse (model, path, func, data)
#	Gnome::ETreeModel	model
#	Gnome::ETreePath	path
#	Gnome::ETreePathFunc	func
#	void *	data

# void
# e_tree_model_node_traverse_preorder (model, path, func, data)
#	Gnome::ETreeModel	model
#	Gnome::ETreePath	path
#	Gnome::ETreePathFunc	func
#	void *	data

#endif

void
e_tree_model_pre_change (tree_model)
	Gnome::ETreeModel	tree_model

void
e_tree_model_node_changed (tree_model, node)
	Gnome::ETreeModel	tree_model
	Gnome::ETreePath	node

void
e_tree_model_node_data_changed (tree_model, node)
	Gnome::ETreeModel	tree_model
	Gnome::ETreePath	node

void
e_tree_model_node_col_changed (tree_model, node, col)
	Gnome::ETreeModel	tree_model
	Gnome::ETreePath	node
	int	col

void
e_tree_model_node_inserted (tree_model, parent_node, inserted_node)
	Gnome::ETreeModel	tree_model
	Gnome::ETreePath	parent_node
	Gnome::ETreePath	inserted_node

void
e_tree_model_node_removed (tree_model, parent_node, removed_node, old_position)
	Gnome::ETreeModel	tree_model
	Gnome::ETreePath	parent_node
	Gnome::ETreePath	removed_node
	int	old_position

#endif

