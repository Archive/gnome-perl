
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "PerlGnomeGalInt.h"

#include "GnomeGalDefs.h"
#include "GtkDefs.h"
#include "GtkTypes.h"

MODULE = Gnome::ETreeTableAdapter		PACKAGE = Gnome::ETreeTableAdapter		PREFIX = e_tree_table_adapter_

#ifdef E_TREE_TABLE_ADAPTER

Gnome::ETableModel
e_tree_table_adapter_new (Class, source)
	SV *	Class
	Gnome::ETreeModel	source
	CODE:
	RETVAL = e_tree_table_adapter_new (source);
	OUTPUT:
	RETVAL

gboolean
e_tree_table_adapter_node_is_expanded (etta, path)
	Gnome::ETreeTableAdapter	etta
	Gnome::ETreePath	path

void
e_tree_table_adapter_node_set_expanded (etta, path, expanded=1)
	Gnome::ETreeTableAdapter	etta
	Gnome::ETreePath	path
	gboolean	expanded

void
e_tree_table_adapter_node_set_expanded_recurse (etta, path, expanded=1)
	Gnome::ETreeTableAdapter	etta
	Gnome::ETreePath	path
	gboolean	expanded

void
e_tree_table_adapter_node_set_visible (etta, path, visible=1)
	Gnome::ETreeTableAdapter	etta
	Gnome::ETreePath	path
	gboolean	visible

Gnome::ETreePath
e_tree_table_adapter_node_at_row (etta, row)
	Gnome::ETreeTableAdapter	etta
	int	row

int
e_tree_table_adapter_row_of_node (etta, path)
	Gnome::ETreeTableAdapter	etta
	Gnome::ETreePath	path

gboolean
e_tree_table_adapter_root_node_is_visible (etta)
	Gnome::ETreeTableAdapter	etta

void
e_tree_table_adapter_root_node_set_visible (etta, visible=1)
	Gnome::ETreeTableAdapter	etta
	gboolean	visible

void
e_tree_table_adapter_show_node (etta, path)
	Gnome::ETreeTableAdapter	etta
	Gnome::ETreePath	path

void
e_tree_table_adapter_save_expanded_state (etta, filename)
	Gnome::ETreeTableAdapter	etta
	char *	filename

void
e_tree_table_adapter_load_expanded_state (etta, filename)
	Gnome::ETreeTableAdapter	etta
	char *	filename

#endif