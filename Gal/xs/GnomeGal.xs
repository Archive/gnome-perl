
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "GnomeGalDefs.h"


static void     callXS (void (*subaddr)(CV* cv), CV *cv, SV **mark)
{
	int items;
	dSP;
	PUSHMARK (mark);
	(*subaddr)(cv);

	PUTBACK;  /* Forget the return values */
}

MODULE = Gnome::Gal	PACKAGE = Gnome::Gal

void
_boot_all ()
	CODE:
	{
#include "GnomeGalobjects.xsh"
	}

void
init (Class)
	SV	*Class
	CODE:
	{
		static int did_it = 0;
		if (did_it)
			return;

		did_it = 1;
		warn("init gal\n");
		GnomeGal_InstallTypedefs();
		GnomeGal_InstallObjects();
		e_cursors_init ();
	}


INCLUDE: ../build/boxed.xsh

INCLUDE: ../build/extension.xsh
