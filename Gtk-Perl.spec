%define name perl-GTK
%define real_name Gtk-Perl
%define version 0.7000
%define release 3mdk

%define dirs . GdkImlib GtkXmHTML GtkGLArea Gnome Glade

Summary: Perl module for the gtk+ library.
Name: %{name}
Version: %{version}
Source: ftp://ftp.gimp.org/pub/gtk/perl/%{real_name}-%{version}.tar.bz2
Patch0: %{real_name}.patch.bz2
Patch1: %{real_name}-fixlibGL.patch.bz2
Release: %{release}
Copyright: GPL
Group: Development/Languages
BuildRoot: /tmp/%{name}-buildroot/
Prefix: %{_prefix}
BuildRequires: perl >= 5.004, gtk+ >= 1.2.1, gnome-libs-devel >= 1.0.54, libglade-devel >= 0.11, imlib-devel >= 1.9.8, gtkglarea-devel >= 1.2.1

%package GdkImlib
Summary: Perl module for the gtk+ GdkImlib library.
Group: Development/Languages

%package XmHTML
Summary: Perl module for the gtk+ XmHTML library.
Group: Development/Languages

%package GLArea
Summary: Perl module for the gtk+ GLArea library.
Group: Development/Languages

%package Gnome
Summary: Perl module for the gtk+ Gnome library.
Group: Development/Languages

%package Glade
Summary: Perl module for the gtk+ Glade library.
Group: Development/Languages

%description
This module lets you use the gtk+ graphic library from perl.
It is very object-oriented and easy to use.

%description GdkImlib
%description XmHTML
%description GLArea
%description Gnome
%description Glade

%prep
%setup -n %{real_name}-%{version}

%patch -p1
%patch1 -p1

%build
for i in %{dirs}; do (
  cd $i
  perl Makefile.PL --lazy-load
  make OPTIMIZE="$RPM_OPT_FLAGS"
) done

%install
rm -rf "$RPM_BUILD_ROOT"
for i in %{dirs}; do (
    cd $i
    make install PREFIX="$RPM_BUILD_ROOT%{_prefix}"
) done

find $RPM_BUILD_ROOT%{_prefix} -name "*.so" | xargs strip

bzip2 -9f $RPM_BUILD_ROOT/usr/lib/perl5/man/man3/*

%files
%defattr(-,root,root)
%dir /%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/Gtk.so
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/Gtk.bs
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/autosplit.ix
%dir %{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/Gdk
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/Gdk/autosplit.ix
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/.packlist
%dir %{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/Install
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/Types.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/Keysyms.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/LogHandler.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/ColorSelectButton.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/Atoms.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/Gdk.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk.pm
%{prefix}/lib/perl5/man/man3/Gtk::GtkColorSelectButton.3.bz2

%files GdkImlib
%defattr(-,root,root)
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/Gdk/ImlibImage
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/Gdk/ImlibImage.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/Gdk/ImlibImage

%files XmHTML
%defattr(-,root,root)
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/XmHTML
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/XmHTML
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/XmHTML.pm
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/GXHTypes.h
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/PerlGtkXmHTMLInt.h
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/GtkXmHTMLDefs.h

%files GLArea
%defattr(-,root,root)
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/GLArea
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/GLArea
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/GLArea.pm

%files Gnome
%defattr(-,root,root)
%doc Gtk/samples/test.pl
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gnome
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gnome
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gnome.pm

%files Glade
%defattr(-,root,root)
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/auto/Gtk/GladeXML
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/GladeXML
%{prefix}/lib/perl5/site_perl/5.005/%{_arch}-linux/Gtk/GladeXML.pm

%changelog
* Wed Jan 26 2000 Pixel <pixel@mandrakesoft.com>
- major rework for 0.7000
- oups % {_arch}
- added test.pl in perl-GTK-gnome

* Tue Nov 23 1999 Pixel <pixel@linux-mandrake.com>
- replaced i386 by %{_arch} (for alpha)

* Sun Nov 21 1999 Pixel <pixel@mandrakesoft.com>
- strip .so (nice rpmlint :)

* Sun Nov  7 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- Build release.

* Mon Oct 18 1999 Pixel <pixel@linux-mandrake.com>
- 0.6123

* Tue Jul 13 1999 Chmouel Boudjnah <chmouel@mandrakesoft.com>
- initialization of spec file.

