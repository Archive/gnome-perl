<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY dblib.dsl
  PUBLIC "-//Norman Walsh//DOCUMENT DSSSL Library V2//EN" CDATA DSSSL>
]>
<style-sheet>
<style-specification id="gpdoc" use="dblib">
<style-specification-body>

(define rgb
        (color-space "ISO/IEC 10179:1996//Color-Space Family::Device RGB"))

(define blue-color (color rgb 0 0 1))
(define red-color (color rgb 1 0 0))
(define black-color (color rgb 0 0 0))
(define light-color (color rgb 0.9 0.9 0.9))

(define %out-font-size% 9pt)
(define %sig-font-size% 9pt)
(define %arg-font-size% 8pt)
(define %adesc-font-size% 7pt)
(define %desc-font-size% 8pt)
(define %head-font-size% 16pt)

(define (string-subst str from to)
	(list->string (map (lambda (c) (if (char=? c from) to c)) (string->list str))))

(define (normalize str)
  (if (string? str)
      (general-name-normalize str
                              (current-node))
      str))

(element doc
	(make simple-page-sequence
		left-margin: 20mm
		right-margin: 20mm
		page-width: 210mm
		page-height: 297mm 
		top-margin: 18mm
		bottom-margin: 14mm
		center-footer: (page-number-sosofo)
;		(make box
;			layer: -1
;			box-type: 'background
;			background-color: light-color
;			(make paragraph
;				layer: -1
;				font-size: 40pt
;				color: light-color
;				(literal "DRAFT")))
		(make paragraph
			space-after: %head-font-size%
			font-size: %head-font-size%
			font-weight: 'bold
			font-family-name: "Helvetica"
			color: black-color
			(literal "Package index"))
		(with-mode pkg-toc (process-matching-children 'package))
		(process-children)
	))

(element type
	(sosofo-append
	(make paragraph
		quadding: 'center
		font-family-name: "Helvetica"
		font-size: %head-font-size%
		font-weight: 'bold
		color: black-color
		keep-with-next?: #t
		space-before: %head-font-size%
		;space-after: 5pt
		keep: 'page
		;(make link destination: (node-list-address (element-with-id "Gtk") (literal "link ")))
		(literal (attribute-string "name")))
	(make rule
		line-thickness: 3pt
		keep-with-next?: #t
		keep-with-previous?: #t
		space-after: (/ %head-font-size% 2)
		)
	(process-children)
	))

(element evalue
	(make paragraph
		(literal (attribute-string "name"))
	))

(element package
	(sosofo-append
	(make paragraph
		quadding: 'center
		font-family-name: "Helvetica"
		font-size: %head-font-size%
		font-weight: 'bold
		color: black-color
		keep-with-next?: #t
		space-before: %head-font-size%
		;space-after: 5pt
		keep: 'page
		;(make link destination: (node-list-address (element-with-id "Gtk") (literal "link ")))
		(literal (attribute-string "name")))
	(make rule
		line-thickness: 3pt
		keep-with-next?: #t
		keep-with-previous?: #t
		space-after: (/ %head-font-size% 2)
		)
	(process-children)
	))

(element method
	(sosofo-append
		(make paragraph
			font-size: %out-font-size%
			font-weight: 'bold
			space-before: %out-font-size%
			keep-with-next?: #t
			;start-indent: (- 10mm)
			font-family-name: "Courier"
			; the color is not inherited by the link with jade
			color: blue-color
			;(literal  (attribute-string "out"))
			(let*
				((endterm (attribute-string "out"))
				(target (element-with-id (string-subst endterm #\: #\-))))
			(make link
				destination: (if (node-list-empty? target) #f (node-list-address target))
				(literal (attribute-string "out") " ")))
		)
		(make paragraph
			font-size: %sig-font-size%
			font-weight: 'bold
			font-family-name: "Courier"
			keep-with-next?: #t
			keep-with-previous?: #t
			keep: 'page
			start-indent: 10mm
			first-line-start-indent: (-  10mm)
			(make sequence
				(with-mode object-arg (process-first-descendant 'arg))
				(literal "->")
				)
			(make sequence
				color: (if (attribute-string "cons") red-color black-color)
				(literal  (attribute-string "name")))
			(make sequence
				font-posture: 'italic
				(literal  " (" (attribute-string "args") ");"))
		)
		(make table
			table-width: 180mm
			keep: 'page
			keep-with-previous?: #t
			font-size: %arg-font-size%
			(make table-column)
			(make table-column)
			(make table-column)
			(with-mode arg-table (process-matching-children 'arg)))
		(process-children)
	))

(element parent 
	(let* 
		; doesn't work....
		((endterm (attribute-string (normalize "name")))
		(target (element-with-id (string-subst endterm #\: #\-))))
	(make paragraph
		quadding: 'center
		(literal "Parent: " )
		(if (node-list-empty? target) (literal (attribute-string "name"))
		;(if (node-list-empty? target) (error (string-append "No element with id: " endterm))
		(make link
			destination: (node-list-address target)
				;(parent (parent (parent (current-node))))
		(literal (attribute-string "name"))))
	)))

(mode pkg-toc
	(element package
		(make paragraph
			(make link
				destination: (current-node-address)
				(literal (attribute-string "name") " ")
				;(make rule orientation: 'escapement)
				(make leader (literal "."))
				(current-node-page-number-sosofo)
			)
		))
	(default (empty-sosofo)))

(mode arg-table
	(element arg
	; skip if it's a package name
	(if (not (or (string=? "." (substring (attribute-string "name") 0 1)) 
			(string=? "$" (substring (attribute-string "name") 0 1)) )) (empty-sosofo)
		(make table-row 
			(make table-cell 
				column-number: 1 
				color: blue-color
				font-family-name: "Courier"
				; background doesn't work with jade
				cell-background?: #t
				background-color: light-color
				;(make paragraph (literal (attribute-string "type") " ")))
				(let*
					((endterm (attribute-string "type"))
					(target (element-with-id (string-subst endterm #\: #\-))))
				(make link
					destination: (if (node-list-empty? target) #f (node-list-address target))
					(literal (attribute-string "type") " "))))
			(make table-cell 
				column-number: 2 
				font-family-name: "Courier"
				font-posture: 'italic
				color: (if (attribute-string "def") red-color black-color)
				(make paragraph (literal " " (attribute-string "name") " ")))
			(make table-cell 
				column-number: 3 
				font-size: %adesc-font-size%
				(make paragraph (literal " " (attribute-string "desc"))))
		)))
	(default (empty-sosofo)))

(mode object-arg
	(element arg
		(make sequence (literal (attribute-string "name")))
	)
	(default (empty-sosofo)))

(element arg
	(empty-sosofo)) 

(element desc
	(make paragraph
		font-size: %desc-font-size%
		keep-with-previous?: #t
		;start-indent: (- 10mm)
	))

(element returns
	(make paragraph
		(literal "Returns: ")
		(process-children)
	))

(element name
	(empty-sosofo))

(element seealso
	(empty-sosofo))

</style-specification-body>
</style-specification>
<external-specification id="dblib" document="dblib.dsl">

</style-sheet>
